# Informations

Ce projet a pour but de tester les connaissances sur Angular pour les candidats postulant chez Attsoft. 

## Inscructions

Pour effectuer vos test et nous les envoyer, vous devez faire un fork du projet et une fois terminé, faire un pull request. On va analyser votre code par la suite et évaluer vos connaissances.
