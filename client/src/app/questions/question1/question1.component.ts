import { Component, OnInit } from '@angular/core';

export class Data{
  nom: string;
  prenom: string;
  numero: string;
}

@Component({
  selector: 'app-question1',
  templateUrl: './question1.component.html',
  styleUrls: ['./question1.component.css']
})
export class Question1Component implements OnInit {

  items: Data[];
  url: string = "http://localhost:4201";

  constructor() { }

  ngOnInit() {
    this.getData();
  }

  getData(){
    //Ajouter le call pour récupérer du data du serveur
  }


}
