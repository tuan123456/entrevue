import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { Question1Component } from './questions/question1/question1.component';
import { Question2Component } from './questions/question2/question2.component';
import { Question3Component } from './questions/question3/question3.component';
import { Question5Component } from './questions/question5/question5.component';
import { Question4Component } from './questions/question4/question4.component';

import {routes} from './app.router';
import { InfoComponent } from './info/info.component';
import { EnfantComponent } from './questions/question2/enfant/enfant.component';
import {HttpClientModule} from "@angular/common/http";

@NgModule({
  declarations: [
    AppComponent,
    Question1Component,
    Question2Component,
    Question3Component,
    Question5Component,
    Question4Component,
    InfoComponent,
    EnfantComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    routes
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
