import {ModuleWithProviders} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {Question1Component} from "./questions/question1/question1.component";
import {Question2Component} from "./questions/question2/question2.component";
import {Question3Component} from "./questions/question3/question3.component";
import {Question4Component} from "./questions/question4/question4.component";
import {Question5Component} from "./questions/question5/question5.component";
import {InfoComponent} from "./info/info.component";

export const router: Routes = [
  {
    path: 'question1', component: Question1Component
  },
  {
    path: 'question2', component: Question2Component
  },
  {
    path: 'question3', component: Question3Component
  },
  {
    path: 'question4', component: Question4Component
  },
  {
    path: 'question5', component: Question5Component
  },
  {
    path: 'home', component: InfoComponent
  },
  {
    path: '**', redirectTo: 'home',
    pathMatch: 'full'
  }
];

export const routes: ModuleWithProviders = RouterModule.forRoot(
  router,
  {enableTracing: false} // <-- debugging purposes only
);
